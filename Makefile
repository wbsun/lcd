
ccflags-y += -Ivirt/kvm -Iarch/x86/kvm
ccflags-m += -Ivirt/kvm -Iarch/x86/kvm

obj-m	+= lcd.o

lcd-objs := lcd_main.o utils.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

insmod4lcd: insmod4lcd.c
	gcc -O2 $^ -o $@

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
	rm -f insmod4lcd *.o
